#######
# Create a bubble chart that compares three other features
# from the mpg.csv dataset. Fields include: mpg, cylinders, displacement, 
# horsepower, weight, accelaration, model_year, origin, name
#######

# this uses the test-env virtual environment
# %%
import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '020_bubble_chart_excercise.html'

# import data
df = pd.read_csv('data/Data/mpg.csv')

print(df.head())

# %%

data = [go.Scatter(x=df['mpg'], 
                   y=df['horsepower'],
                   text=df['name'],
                   mode='markers',
                   marker=dict(size=df['mpg']* 1.5,
                    color=df['weight'],
                    showscale=True
                    )
                  )]

layout = go.Layout(title='Cylinders to Horsepower', hovermode='y')

fig = go.Figure(data=data, layout=layout)

pyo.plot(fig, filename=file_name)