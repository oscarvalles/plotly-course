# this uses the test-env virtual environment
import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '013_linechart.html'

# read in data
df = pd.read_csv('data/SourceData/nst-est2017-alldata.csv')
df2 = df[df['DIVISION'] == '1'] # filter to NE states
df2.set_index('NAME', inplace=True)


# filter out only the population columns
list_of_pop_columns = [col for col in df2.columns if col.startswith('POP')]
df2 = df2[list_of_pop_columns]

data = [go.Scatter(x='df.columns', 
                   y=df2.loc[name], 
                   mode='lines', 
                   name=name) for name in df2.index]

pyo.plot(data)
