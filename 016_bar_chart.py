######################
# Create a stacked bar chart from the file "mocksurvey.csv"
# Note: questions appear in the index (s/b used for x-axis)
#       responses should appear as column labels
# Additional Challenge: Make chart horizontal
######################

# this uses the test-env virtual environment

import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '016_bar_chart_exercise.html'

df = pd.read_csv('data/Data/mocksurvey.csv', index_col=0)
df.head()

### this returns a regular horizontal bar chart
# data = [go.Bar(x=df.index, 
#                y=df[col], 
#                name=col
#               ) for col in df.columns]

### the following makes the chart horizontal (swap x & y, add param)
data = [go.Bar(x=df[col], 
               y=df.index, 
               name=col,
               orientation='h'
              ) for col in df.columns]


layout = go.Layout(title='Survey Responses', 
                   barmode='stack'
                   )

figure = go.Figure(data=data, layout=layout)

pyo.plot(figure, filename=file_name)
