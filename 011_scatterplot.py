# this uses the test-env virtual environment

import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '011_scatterplot.html'

np.random.seed(42)

random_x = np.random.randint(1,101,100)
random_y = np.random.randint(1,101,100)


# contains the actual data
data = [go.Scatter(x=random_x, 
                   y=random_y, 
                   mode='markers',
                   marker=dict(
                       size=12,
                       color='rgb(51,204,153)',
                       symbol='pentagon',
                       line= {'width':2}
                   ))]

# determines how the data is laid out
layout = go.Layout(title="Random Scatter Plot",
                   xaxis={'title':'My X Axis'},
                   yaxis=dict(title='My Y Axis'), #dict is usually preferred over an actual dictionary
                   hovermode='closest') 

# stores data and layout into one variable
fig = go.Figure(data=data, layout=layout)

# plot the information and set the filename
pyo.plot(fig, filename=file_name)