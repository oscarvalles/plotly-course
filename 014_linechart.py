##################
# Using file 2010YumaAZ.csv, develop a line chart
# that plots seven days worth of temp data on one graph
# You can use a for loop to assign each day to its own trace
##################

# this uses the test-env virtual environment
import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '014_linechart-homework.html'


# read in data
df = pd.read_csv('data/Data/2010YumaAZ.csv')
df.head()

# create unique days list
days = df['DAY'].unique().tolist()

# create a list to hold the traces/lines for the chart
data = [go.Scatter(x=df['LST_TIME'],
                   y=df[df['DAY']==day]['T_HR_AVG'], # note the day filter
                   mode='lines',
                   name=day
                   ) for day in days]

layout = go.Layout(title='Average Daily Temperatures')
figure = go.Figure(data=data, layout=layout)

pyo.plot(figure, filename=file_name)