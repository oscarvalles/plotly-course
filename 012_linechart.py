# this uses the test-env virtual environment

import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '012_linechart.html'
np.random.seed(56)

x_values = np.linspace(0,1,100)
y_values = np.random.randn(100)


# every single line that we place on our figure, is called a trace
trace01 = go.Scatter(x=x_values, y=y_values+5, 
                   mode='markers', 
                   name='markers')

trace02 = go.Scatter(x=x_values, y=y_values, 
                   mode='lines', 
                   name='my lines')


trace03 = go.Scatter(x=x_values, y=y_values-5, 
                   mode='lines+markers', 
                   name='My favorite')

# now we can store one or more series into a list
data = [trace01, trace02, trace03]


layout = go.Layout(title="line charts")

figure = go.Figure(data=data, layout=layout)

pyo.plot(figure, filename=file_name)