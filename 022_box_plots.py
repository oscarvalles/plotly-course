# Box Plot Examples

import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '022_box_plots.html'
