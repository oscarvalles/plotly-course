# this uses the test-env virtual environment
import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '015_bar_charts.html'

df = pd.read_csv('data/Data/2018WinterOlympics.csv')

medal_levels = ['Gold','Silver','Bronze']

#### to plot single bar ####
# data = [go.Bar(x=df['NOC'], y=df['Total'])]

#### to plot multiple bars ####
data = [go.Bar(x=df['NOC'], 
               y=df[ml],
               marker={'color':ml}
               ) for ml in medal_levels]


#### to plot a nested bar chart, i.e. multiple bars ####
# layout = go.Layout(title='Medals')

### to plot a stacked bar chart ###
layout = go.Layout(title='Medals by Country', barmode='stack')

figure = go.Figure(data=data, layout=layout)

pyo.plot(figure, filename=file_name)