# this uses the test-env virtual environment

import pandas as pd
import numpy as np
import plotly.offline as pyo 
import plotly.graph_objs as go

import os
dir_path = os.getcwd() + '/generated-charts/'
file_name = dir_path + '019_bubble_chart.html'

# read in data
df = pd.read_csv('data/Data/mpg.csv')

data = [go.Scatter(x=df['horsepower'],
                   y=df['mpg'],
                   text=df['name'],
                   mode='markers',
                   marker=dict(size=df['weight']/100,  # controls the size of the bubble
                               color=df['cylinders'],  # breaks out colors by this feature
                               showscale=True          # displays a color bar
                              ) 

                   )]

layout = go.Layout(title='MPG to Horsepower',
                   hovermode='y' # determines what to display (x, y, & closest are some options)
                  )

fig = go.Figure(data=data, layout=layout)

pyo.plot(fig, filename=file_name)